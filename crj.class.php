<?php

/**
 * CR-J
 * Simple CMS using JSON file as a database
 *
 * @version 0.2
 * @package CR-J
 * @author d-bo <>
 * @license http://opensource.org/licenses/MIT
 */

class crj
{
    /**
     * JSON decoded array
     *
     * @var array
     */
    protected $data;
    
    /**
     * Input configuration storage
     *
     * @var array
     */
    protected $config = array();
    
    /**
     * Decode pages from database
     *
     * @var array
     */
    protected $pages;
    
    /**
     * If nested from root (url dependency)
     *
     * @var string
     */
    protected $urlOffsetFrom;
    
    /**
     * Plugin objects container
     *
     * @var array
     */
    protected $pluginObj = array();
    
    /**
     * Template variables
     *
     * @var array
     */
    protected $variables;
    
    /**
     * File storage URL route
     *
     * @var string
     */
    protected $file_storage_url;
    
    /**
     * Constructor. Prepare environment.
     *
     * @param array $config Passing config data when creating object
     */
    public function __construct($config)
    {
        $this->config = $config;
        
        # default is the current working directory
        $curDir = getcwd();
        $path = $curDir . DIRECTORY_SEPARATOR . 'data.json';
        
        if (is_file($path)) {
            # data.json present
            $data = file_get_contents($path);
            $this->data = json_decode($data);
        } else {
            # try to create default data.json
            $default = array();
            $time = time();
            $default[] = array(
                "type"    => "deploy",
                "created" => $time
            );
            $dir = $curDir . DIRECTORY_SEPARATOR . "data.json";
            if (!@file_put_contents($dir, json_encode($default))) {
                $this->error(0);
            }
        }
        
        # running root or not
        if ($config['is-running-root']) {
            $this->is_running_root();
        }
        
        # file storage url prefix
        if (isset($config['file-storage-url'])) {
            $this->file_storage_url = $config['file-storage-url'];
        }
        
        # init database
        # TODO: split database process from initial thread
        $this->getDB();
        
        # construct admin template
        if (is_string($config['admin-url'])) {
            $this->constructAdmin($config['admin-url']);
        }
        
        $this->route();
    }
    
    /**
     * Get all the records from db
     * Save all the data in $pages class variable
     */
    public function getDB()
    {
        # get all records
        $path = getcwd() . DIRECTORY_SEPARATOR . 'data.json';
        $this->pages = $this->getAll($path);
    }
    
    /**
     * Running not from a root directory ?
     * example: http://site.com/page/
     * maybe it runs from a dir /var/www/site/page/ directory ?
     * it calls index.php anyway. we need to detect it.
     *
     * Affects $urlOffsetFrom class variable
     */
    public function is_running_root()
    {
        $chunk = explode('/', getcwd());
        $count = count($chunk);
        $last_chunk_dir = $chunk[$count - 1];
        
        $chunk  = explode('/', "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
        $count = count($chunk);
        $last_chunk_url = $chunk[$count - 2];
        if ($last_chunk_dir == $last_chunk_url) {
            $this->urlOffsetFrom = $last_chunk_url; 
        } else {
            $this->urlOffsetFrom = false;
        }
    }
    
    /**
     * Parse URL
     *
     * @return array Parsed url
     */
    public function parseUrl()
    {
        $http = !empty($_SERVER['HTTPS']) ? 'https://' : 'http://';
        $full = $http . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        
        // avoid url file collision
        if (strpos($full, $_SERVER['PHP_SELF'])) {
            $full = str_replace($_SERVER['PHP_SELF'], '', $full);
        }
        $path = parse_url($full);
        if ($this->urlOffsetFrom) {
            $path['path'] = str_replace('/' . $this->urlOffsetFrom, '', $path['path']);
        }

        $_path = explode('/', $path['path']);
        for ($i = 1; $i < count($_path); $i++) {
            if (!empty($_path[$i])) {
                $parsed[] = $_path[$i];
            }
        }
        
        $this->variables['SYS_URL'] = $full;
        
        return $parsed;
    }
    
    /**
     * Assign variable to use in template engine
     * Use class variable $variables to collect the data
     *
     * @param string $var Variable name
     * @param string $value Variable value
     */
    public function assign($var, $value)
    {
        if (isset($var) && isset($value)) {
            $this->variables[$var] = $value;
        }
    }
    
    /**
     * Get template variable
     *
     * @param string $var Variable name
     *
     * @return string Variable value
     */
    public function get($var)
    {
        if ($var) {
            return $this->variables[$var];
        }

        return false;
    }
    
    /**
     * File output stream
     * 
     * @param string $filename File name to search and output
     */
    
    public function pushFile($filename)
    {
        $flag = false;
        
        if (strlen($filename) > 0) {
            foreach ($this->pages as $k => $v) {
                if ($v['type'] == 'file' && $v['name'] == urldecode($filename)) {
                    # first - search in system directory
                    $path = dirname(__FILE__) . DIRECTORY_SEPARATOR . $v['folder'] . DIRECTORY_SEPARATOR;
                    #$this->isDirectoryTraversal($path, $filename);
                    
                    if (!is_file($path . urldecode($filename))) {
                        # search in working directory from config path
                        $path = getcwd() . DIRECTORY_SEPARATOR . $v['folder'] . DIRECTORY_SEPARATOR;
                        //$this->isDirectoryTraversal($path, $filename);
                        
                        if (!is_file($path  . urldecode($filename))) {
                            echo 'no file <b style="font-size: 22px">' . $path  . urldecode($filename) . '</b>';
                            exit;
                        }
                    }
                    
                    $flag = true;
                }
            }
            
            if (!$flag) {
                $this->error(2);
                exit;
            }
            
            # TODO: detect mime-type and put into response header
            //header('Content-Type: text/css');
            
            $mime_type = $this->detectMIME($filename);
            if (!$mime_type) $mime_type = 'application/octet-stream';
            header('Content-Type: ' . $mime_type);
            
            if (!readfile($path . urldecode($filename))) {
                $this->error(3);
                exit;
            }
            
            exit;
        }
    }
    
    /**
     * Detect directory traversal vulnerability
     * (deprecated)
     * !!! realpath() NOT WORKING when not have executing privileges 
     */
    public function isDirectoryTraversal($dir, $param)
    {
        
    }
    
    /**
     * Scan directory for files
     *
     * @param string $input Path to directory
     *
     * @return string JSON encoded string
     */
    public function scan($input)
    {
        $dir = new DirectoryIterator($input);
        $collect = array();
        foreach ($dir as $fileinfo) {
            if (!$fileinfo->isDot()) {
                $filename = $fileinfo->getFilename();
                $chunk    = explode('-', $filename);
                $size     = count($chunk);
                $key      = str_replace('.jpg', '', $chunk[$size - 1]);
                $collect[$key] = $filename;
            }
        }
        
        asort($collect);
        
        $i = 0;
        foreach ($collect as $k => $v) {
            $out[$i] = $v;
            $i++;
        }
        
        echo json_encode($out);
    }
    
    /**
     * URL router
     *
     * @return string Rendered html source
     */
    public function route()
    {
        $db = $this->pluginObj["database"];
        $url_chunks = $this->parseUrl();
        
        # check for file storage redirect
        if (isset($this->file_storage_url) && !empty($url_chunks)) {
            if (in_array(mb_strtolower($this->file_storage_url), $url_chunks)) {
                $filename = urldecode($_GET['p']);
                $this->pushFile($filename);
                exit;
            }
        }

        if (count($url_chunks) > 0) {
            foreach ($this->pages as $k => $v) {
                if ($v['type'] == 'page' && mb_strtolower($v['url']) == mb_strtolower($url_chunks[0])) {
                    $obj = $v;
                    break;
                }
            }
            
            if (!isset($obj)) {
                echo '404'; exit;
            }
        } else {
            # show index page
            foreach ($this->pages as $k => $v) {
                if ($v['type'] == 'page' && $v['url'] == '/') {
                    $obj = $v;
                    break;
                }
            }
        }
        
        $this->assign('TPL_PAGE_ID', $obj['id']);
        $this->assign('TPL_PAGE_NAME', $obj['name']);
        
        $id = $obj['htmlBlock'];
        $string = $this->pages[$id]['html'];
        
        if ($this->config['debug-vars']) {
            $this->debugVars();
        }
        
        $tpl = new tpl($this->variables, $this->pages, $this->config);
        echo $tpl->render($string);
        
        exit;
    }
    
    /**
     * Debug inner variables
     */
    public function debugVars()
    {
        echo '<div style="position: relative">';
        echo '<div style="position: absolute;right: 10px;top: 0;background: #ccc;z-index: 1000;color: #fff;padding:3px 10px;font-size: 13px">inner variables debug</div>';
        echo '<div style="position: absolute;right: 5px;top: 0;width: 300px;margin: 10px 0;padding: 10px;border: 1px solid #ccc;font-size: 12px;background: #fff">';
        foreach ($this->variables as $k => $v) {
            echo $k . ' => <b style="color: green">' . $v . '</b><br>';
        }
        echo '</div></div>';
    }
    
    /**
     * Show error
     *
     * @param int $id Error type id
     * @param bool $exit Flag to exit after error rendering
     */
    public function error($id, $exit = false)
    {
        # TODO: language specific errors file
        $errors = array(
            0 => "Cannot create <b>data.json</b>. Maybe no privileges ?",
            1 => "No database wrapper created.",
            2 => "No file in storage",
            3 => "Cannot read file"
        );
        
        $advice = array(
            0 => "Try to chmod current working directory to write & read",
            1 => "include query.class.php and make an object in pluginObj[\"database\"]",
            2 => "Check filename",
            3 => "No privileges ?"
        );
        
        echo '<body style="background: #f0f0f0">';
        echo '<div style="padding: 30px;border: 1px solid #ccc;margin: 20px;font-size: 18px;display: inline-block;background: #fff">';
        echo $errors["$id"];
        echo '<div style="padding: 5px;border-left: 1px dotted green;font-size: 14px;margin: 5px;">' . $advice["$id"] . '</div>';
        echo '</div>';
        echo '</body>';
        
        if ($exit === true) exit(); else return;
    }
    
    /**
     * Retrieve db records
     *
     * @param string $path Path to .json database file
     * @return array All the records from parsed JSON
     */
    public function getAll($path)
    {
        $data = file_get_contents($path);
        $this->db = json_decode($data);
        
        foreach ($this->db as $k => $v) {
            foreach ($v as $z => $x) {
                $out[$v->id][$z] = $x;
            }
        }
        
        return $out;
    }
    
    /**
     * Construct admin template
     *
     * @param string $url Admin url from config
     */
    
    public function constructAdmin($url)
    {
        $this->pages['admin']['url'] = $url;
        
        $tpl = $this->config['root'] . DIRECTORY_SEPARATOR . 'admin.tpl';
        $this->pages['admin-page']['html'] = file_get_contents($tpl);
        
        # generate page json array for admin usage
        foreach ($this->pages as $k => $v) {
            if ($v['type'] == 'page') {
                $out[] = $v['name'];
            }
        }
        
        $this->variables['SYS_ADMIN_PAGES'] = $this->getIndexedPages();
        $this->variables['SYS_ADMIN_FILES'] = $this->getIndexedFiles();
    }
    
    /**
     * Get indexed files from database
     *
     * @return string JSON encoded files
     */
    
    public function getIndexedFiles()
    {
        foreach ($this->pages as $k => $v) {
            if ($v['type'] == 'file') {
                $folder = $v['folder'];
                $out[$folder][] = $v['name'];
            }
        }
        
        return json_encode($out);
    }
    
    /**
     * Get indexed pages from database
     *
     * @return string JSON encoded pages
     */
    
    public function getIndexedPages()
    {
        foreach ($this->pages as $k => $v) {
            if ($v['type'] == 'page') {
                $out[] = $v['name'];
            }
        }
        
        return json_encode($out);
    }
    
    /**
     * Detect filename MIME-type
     *
     * @param string $filename Input file name
     *
     * @return string|bool MIME-type or false if not detected
     */
    
    public function detectMIME($filename)
    {
        $extensionToType = array (
            'ez'        => 'application/andrew-inset',
            'atom'      => 'application/atom+xml',
            'jar'       => 'application/java-archive',
            'hqx'       => 'application/mac-binhex40',
            'cpt'       => 'application/mac-compactpro',
            'mathml'    => 'application/mathml+xml',
            'doc'       => 'application/msword',
            'dat'       => 'application/octet-stream',
            'oda'       => 'application/oda',
            'ogg'       => 'application/ogg',
            'pdf'       => 'application/pdf',
            'ai'        => 'application/postscript',
            'eps'       => 'application/postscript',
            'ps'        => 'application/postscript',
            'rdf'       => 'application/rdf+xml',
            'rss'       => 'application/rss+xml',
            'smi'       => 'application/smil',
            'smil'      => 'application/smil',
            'gram'      => 'application/srgs',
            'grxml'     => 'application/srgs+xml',
            'kml'       => 'application/vnd.google-earth.kml+xml',
            'kmz'       => 'application/vnd.google-earth.kmz',
            'mif'       => 'application/vnd.mif',
            'xul'       => 'application/vnd.mozilla.xul+xml',
            'xls'       => 'application/vnd.ms-excel',
            'xlb'       => 'application/vnd.ms-excel',
            'xlt'       => 'application/vnd.ms-excel',
            'xlam'      => 'application/vnd.ms-excel.addin.macroEnabled.12',
            'xlsb'      => 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
            'xlsm'      => 'application/vnd.ms-excel.sheet.macroEnabled.12',
            'xltm'      => 'application/vnd.ms-excel.template.macroEnabled.12',
            'docm'      => 'application/vnd.ms-word.document.macroEnabled.12',
            'dotm'      => 'application/vnd.ms-word.template.macroEnabled.12',
            'ppam'      => 'application/vnd.ms-powerpoint.addin.macroEnabled.12',
            'pptm'      => 'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
            'ppsm'      => 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
            'potm'      => 'application/vnd.ms-powerpoint.template.macroEnabled.12',
            'ppt'       => 'application/vnd.ms-powerpoint',
            'pps'       => 'application/vnd.ms-powerpoint',
            'odc'       => 'application/vnd.oasis.opendocument.chart',
            'odb'       => 'application/vnd.oasis.opendocument.database',
            'odf'       => 'application/vnd.oasis.opendocument.formula',
            'odg'       => 'application/vnd.oasis.opendocument.graphics',
            'otg'       => 'application/vnd.oasis.opendocument.graphics-template',
            'odi'       => 'application/vnd.oasis.opendocument.image',
            'odp'       => 'application/vnd.oasis.opendocument.presentation',
            'otp'       => 'application/vnd.oasis.opendocument.presentation-template',
            'ods'       => 'application/vnd.oasis.opendocument.spreadsheet',
            'ots'       => 'application/vnd.oasis.opendocument.spreadsheet-template',
            'odt'       => 'application/vnd.oasis.opendocument.text',
            'odm'       => 'application/vnd.oasis.opendocument.text-master',
            'ott'       => 'application/vnd.oasis.opendocument.text-template',
            'oth'       => 'application/vnd.oasis.opendocument.text-web',
            'potx'      => 'application/vnd.openxmlformats-officedocument.presentationml.template',
            'ppsx'      => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
            'pptx'      => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'xlsx'      => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'xltx'      => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
            'docx'      => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'dotx'      => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
            'vsd'       => 'application/vnd.visio',
            'wbxml'     => 'application/vnd.wap.wbxml',
            'wmlc'      => 'application/vnd.wap.wmlc',
            'wmlsc'     => 'application/vnd.wap.wmlscriptc',
            'vxml'      => 'application/voicexml+xml',
            'bcpio'     => 'application/x-bcpio',
            'vcd'       => 'application/x-cdlink',
            'pgn'       => 'application/x-chess-pgn',
            'cpio'      => 'application/x-cpio',
            'csh'       => 'application/x-csh',
            'dcr'       => 'application/x-director',
            'dir'       => 'application/x-director',
            'dxr'       => 'application/x-director',
            'dvi'       => 'application/x-dvi',
            'spl'       => 'application/x-futuresplash',
            'tgz'       => 'application/x-gtar',
            'gtar'      => 'application/x-gtar',
            'hdf'       => 'application/x-hdf',
            'js'        => 'application/x-javascript',
            'skp'       => 'application/x-koan',
            'skd'       => 'application/x-koan',
            'skt'       => 'application/x-koan',
            'skm'       => 'application/x-koan',
            'latex'     => 'application/x-latex',
            'nc'        => 'application/x-netcdf',
            'cdf'       => 'application/x-netcdf',
            'sh'        => 'application/x-sh',
            'shar'      => 'application/x-shar',
            'swf'       => 'application/x-shockwave-flash',
            'sit'       => 'application/x-stuffit',
            'sv4cpio'   => 'application/x-sv4cpio',
            'sv4crc'    => 'application/x-sv4crc',
            'tar'       => 'application/x-tar',
            'tcl'       => 'application/x-tcl',
            'tex'       => 'application/x-tex',
            'texinfo'   => 'application/x-texinfo',
            'texi'      => 'application/x-texinfo',
            't'         => 'application/x-troff',
            'tr'        => 'application/x-troff',
            'roff'      => 'application/x-troff',
            'man'       => 'application/x-troff-man',
            'me'        => 'application/x-troff-me',
            'ms'        => 'application/x-troff-ms',
            'ustar'     => 'application/x-ustar',
            'src'       => 'application/x-wais-source',
            'xhtml'     => 'application/xhtml+xml',
            'xht'       => 'application/xhtml+xml',
            'xslt'      => 'application/xslt+xml',
            'xml'       => 'application/xml',
            'xsl'       => 'application/xml',
            'dtd'       => 'application/xml-dtd',
            'zip'       => 'application/zip',
            'au'        => 'audio/basic',
            'snd'       => 'audio/basic',
            'mid'       => 'audio/midi',
            'midi'      => 'audio/midi',
            'kar'       => 'audio/midi',
            'mpga'      => 'audio/mpeg',
            'mp2'       => 'audio/mpeg',
            'mp3'       => 'audio/mpeg',
            'aif'       => 'audio/x-aiff',
            'aiff'      => 'audio/x-aiff',
            'aifc'      => 'audio/x-aiff',
            'm3u'       => 'audio/x-mpegurl',
            'wma'       => 'audio/x-ms-wma',
            'wax'       => 'audio/x-ms-wax',
            'ram'       => 'audio/x-pn-realaudio',
            'ra'        => 'audio/x-pn-realaudio',
            'rm'        => 'application/vnd.rn-realmedia',
            'wav'       => 'audio/x-wav',
            'pdb'       => 'chemical/x-pdb',
            'xyz'       => 'chemical/x-xyz',
            'bmp'       => 'image/bmp',
            'cgm'       => 'image/cgm',
            'gif'       => 'image/gif',
            'ief'       => 'image/ief',
            'jpeg'      => 'image/jpeg',
            'jpg'       => 'image/jpeg',
            'jpe'       => 'image/jpeg',
            'png'       => 'image/png',
            'svg'       => 'image/svg+xml',
            'tiff'      => 'image/tiff',
            'tif'       => 'image/tiff',
            'djvu'      => 'image/vnd.djvu',
            'djv'       => 'image/vnd.djvu',
            'wbmp'      => 'image/vnd.wap.wbmp',
            'ras'       => 'image/x-cmu-raster',
            'ico'       => 'image/x-icon',
            'pnm'       => 'image/x-portable-anymap',
            'pbm'       => 'image/x-portable-bitmap',
            'pgm'       => 'image/x-portable-graymap',
            'ppm'       => 'image/x-portable-pixmap',
            'rgb'       => 'image/x-rgb',
            'xbm'       => 'image/x-xbitmap',
            'psd'       => 'image/x-photoshop',
            'xpm'       => 'image/x-xpixmap',
            'xwd'       => 'image/x-xwindowdump',
            'eml'       => 'message/rfc822',
            'igs'       => 'model/iges',
            'iges'      => 'model/iges',
            'msh'       => 'model/mesh',
            'mesh'      => 'model/mesh',
            'silo'      => 'model/mesh',
            'wrl'       => 'model/vrml',
            'vrml'      => 'model/vrml',
            'ics'       => 'text/calendar',
            'ifb'       => 'text/calendar',
            'css'       => 'text/css',
            'csv'       => 'text/csv',
            'html'      => 'text/html',
            'htm'       => 'text/html',
            'txt'       => 'text/plain',
            'asc'       => 'text/plain',
            'rtx'       => 'text/richtext',
            'rtf'       => 'text/rtf',
            'sgml'      => 'text/sgml',
            'sgm'       => 'text/sgml',
            'tsv'       => 'text/tab-separated-values',
            'wml'       => 'text/vnd.wap.wml',
            'wmls'      => 'text/vnd.wap.wmlscript',
            'etx'       => 'text/x-setext',
            'mpeg'      => 'video/mpeg',
            'mpg'       => 'video/mpeg',
            'mpe'       => 'video/mpeg',
            'qt'        => 'video/quicktime',
            'mov'       => 'video/quicktime',
            'mxu'       => 'video/vnd.mpegurl',
            'm4u'       => 'video/vnd.mpegurl',
            'flv'       => 'video/x-flv',
            'asf'       => 'video/x-ms-asf',
            'asx'       => 'video/x-ms-asf',
            'wmv'       => 'video/x-ms-wmv',
            'wm'        => 'video/x-ms-wm',
            'wmx'       => 'video/x-ms-wmx',
            'avi'       => 'video/x-msvideo',
            'ogv'       => 'video/ogg',
            'movie'     => 'video/x-sgi-movie',
            'ice'       => 'x-conference/x-cooltalk',
        );
        
        $chunks = @explode('.', $filename);
        $count = count($chunks);
        
        if ($count > 0) {
            $extension = mb_strtolower($chunks[$count-1]);
            if (array_key_exists($extension, $extensionToType)) {
                return $extensionToType[$extension];
            }
        } else {
            return false;
        }
    }
    
}


/**
 * Template parser class
 *
 * @version 0.3
 * @author d-bo <>
 */

class tpl
{
    var $variables = array();
    var $pages = array();
    var $config = array();
    
    /**
     * tpl Constructor
     *
     * @param array $vars Array of keys (variable names) and values
     * @param array $pages Pages from cr-j
     * @param array $config Config from cr-j
     *
     * TODO: take out cr-j arrays from tpl class. make it independent.
     */
    public function __construct($vars, $pages, $config = array())
    {
        $this->variables = $vars;
        $this->pages = $pages;
        $this->config = $config;
    }
    
    /**
     * Render template string
     *
     * @param string $string Data to parse
     * @param array $variables A set of variables for replacement in templates
     */
    public function render($string, $is_page = false)
    {
        # it isn't really perfect to hold an html data in json string
        # we need to encode special characters and extend file length
        $string = urldecode($string);
        if ($is_page) {
            //echo $string; exit;
            if (is_numeric($string)) {
                $str = $this->pages[$string]['html'];
            } else {
                foreach ($this->pages as $k => $v) {
                    if ($v['type'] == 'htmlBlock' && $v['name'] == $string) {
                        $str = $v['html'];
                    }
                }
            }
        } else {
            $str = $string;
        }
        
        $pattern = "/\{\{(.*?)\}\}/";
        $found = preg_match_all($pattern, $str, $matches);
        
        if ($found > 0) {
            $i = 0;
            # iterate matched blocks
            foreach ($matches[1] as $k => $v) {
                $str = $this->getContainerAction($v, $matches[0][$i], trim($str), $matches[0], $i);
                $i++;
            }
        }
        
        return $str;
    }
    
    /**
     * Render {{ container }} specific action
     * 
     * @param string $str Container string without {{ }} mustaches
     * @param string $origstr Container string including mustaches
     * @param string $fullstr Original string
     * @param array $matches Matched by regular expression blocks (with mustaches)
     * @param int $container_number Current container id from $matches array
     * @return string Processed string
     */
    
    public function getContainerAction($str, $origstr, $fullstr, $matches, $container_number)
    {
        switch($str) {
            # include external page (may be recursive)
            # example: {{ include="first_page" }}
            case(strpos($str, 'include') !== false):
                $chunk = explode('=', $str);
                if (!strpos($chunk[1], '"')) $repl = '"'; else $repl = "'";
                $_str = str_replace($repl, '', $chunk[1]);
                $file = $this->render(trim($_str, "'\""), true);
                $fullstr = str_replace($origstr, $file, $fullstr);
                return $fullstr;
                break;
            
            # search for file in database records and render a link
            # !!! file must allready recorder in database before rendering
            # example: {{ file="main.js" }}
            case(strpos($str, 'file') !== false):
                $chunk = explode('=', $str);
                if (!strpos($chunk[1], '"')) $repl = '"'; else $repl = "'";
                $_str = str_replace($repl, '', $chunk[1]);
                
                # search for file by id or by name
                if (is_numeric($_str)) {
                    # id
                    foreach ($this->pages as $k => $v) {
                        if ($v['type'] == 'file' && $v['id'] == $_str) {
                            # match !
                            $replace = $v['id'];
                        }
                    }
                } else {
                    # filename
                    foreach ($this->pages as $k => $v) {
                        if ($v['type'] == 'file' && $v['name'] == urldecode($_str)) {
                            # match
                            $replace = urldecode($v['name']);
                        }
                    }
                }
                
                $fullstr = str_replace($origstr, $this->config['file-storage-url'] . '?p=' . $replace, $fullstr);
                return $fullstr;
                break;
                
            # render link of a page recorder in database
            # example: {{link="first_page"}}
            case(strpos($str, 'link') !== false):
                $chunk = explode('=', $str);
                if (!strpos($chunk[1], '"')) $repl = '"'; else $repl = "'";
                $_str = str_replace($repl, '', $chunk[1]);
                
                $http = !empty($_SERVER['HTTPS']) ? 'https://' : 'http://';
                $host = $http . $_SERVER['HTTP_HOST'];
                
                if (is_numeric($_str)) {
                    # searching for .json block by id
                    if ($this->pages[$_str]['type'] == 'page') {
                        $fullstr = str_replace($origstr, $this->variables['SYS_URL'] . '/' . $this->pages[$_str]['url'], $fullstr);
                    }
                } else {
                    //echo $host; exit;
                    // searching for block attribute by name
                    foreach ($this->pages as $k => $v) {
                        if ($v['type'] == 'page' && $v['name'] == $_str) {
                            $fullstr = str_replace($origstr, $host . '/' . $v['url'], $fullstr);
                        }
                    }
                }
                
                return $fullstr;
                break;
                
            # "if" logic block processing
            # example:
            # {{if $val="pepe"}}
            # ...
            # {{elseif}}, {{else}}
            # ...
            # {{/if}} or {{endif}}
            
            case((strpos(strtolower($str), 'if') !== false && strpos($str, '/') === false && strpos($str, 'else') === false)):
                
                # detect "="
                # TODO: detect ">", "<", "!="
                //echo 'if: <b>' . $str . '</b>';
                if (strpos($str, '>')) {
                    $operation = 'more_than'; $sep = '>';
                }
                if (strpos($str, '<')) {
                    $operation = 'lower_than'; $sep = '<';
                }
                if (strpos($str, '=')) {
                    $operation = 'equal'; $sep = '=';
                }
                if (strpos($str, '!=')) {
                    $operation = 'not_equal'; $sep = '!=';
                }
                
                $chunk = explode($sep, $str);
                
                # cut "if", "$"
                # detect variable name
                $variable = trim($chunk[0], "IFif$ ");
                
                # cut "'
                # detect compare parameter
                $compare_param = trim($chunk[1], "\"' ");
                
                # compare condition
                if (isset($this->variables["$variable"])) {
                    switch($operation) {
                        case("more_than"):
                            if ($compare_param > $this->variables["$variable"]) $flag = true; else $flag = false;
                            break;
                            
                        case("lower_than"):
                            if ($compare_param < $this->variables["$variable"]) $flag = true; else $flag = false;
                            break;
                            
                        case("equal"):
                            if ($compare_param == $this->variables["$variable"]) $flag = true; else $flag = false;
                            break;
                            
                        case("not_equal"):
                            if ($compare_param != $this->variables["$variable"]) $flag = true; else $flag = false;
                            break;
                    }
                } else {
                    $error = "<b style=\"color: brown;font-size: 20px;border: 1px solid #ccc\">\$$variable</b> variable is not defined";
                }
                
                if ($flag) {
                    # satisfy condition
                    # render condition block
                    # render text after {{if}} before {{elseif}}
                    $start_pos = (strpos($fullstr, $matches[$container_number])) + strlen($matches[$container_number]);
                    for ($i = $container_number; $i <= count($matches); $i++) {
                        if (strpos($matches[$i], "elseif") || strpos($matches[$i], "else")) {
                            $end_pos = strpos($fullstr, $matches[$i]) - 1;
                            break;
                        }
                    }
                } else {
                    # not satisfied
                    # render text after {{elseif}} before {{/if}}
                    $start_pos = (strpos($fullstr, $matches[$container_number + 1]) + strlen($matches[$container_number + 1]));
                    $end_pos = strpos($fullstr, $matches[$container_number + 2]);
                }
                
                # string to replace whole logic {{if, elseif, /if}} block with
                $substr = substr($fullstr, $start_pos, ($end_pos - $start_pos));
                # render string for template variables
                $substr = $this->render($substr, false);
                
                # retrieve begin and end of the whole logic block
                $_start = strpos($fullstr, $matches[$container_number]);
                #search for {{/if}}
                for ($i = $container_number; $i <= count($matches); $i++) {
                    if (strpos($matches[$i], "/if") || strpos($matches[$i], "endif")) {
                        $_end = strpos($fullstr, $matches[$i]) + strlen($matches[$i]);
                        break;
                    }
                }
                
                $_string = substr($fullstr, $_start, ($_end - $_start));
                
                if ($error) $fullstr = str_replace($_string, $error, $fullstr);
                else $fullstr = str_replace($_string, $substr, $fullstr);
                
                return $fullstr;
                
                break;
                
            # render simple variable
            # !!! must be at the end of this switch container
            case(strpos($str, '$') !== false):
                
                $chunk = explode('$', $str);
                foreach ($this->variables as $k => $v) {
                    $val = trim($chunk[1]);
                    if ($val == $k) {
                        $fullstr = str_replace($origstr, $v, $fullstr);
                    }
                }
                
                return $fullstr;
                break;
                
            # no action found. return original string
            default:
                return $fullstr;
                break;
        }
    }
}