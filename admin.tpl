<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru">
<base href="http://test.polus-neft.ru/">
<head>
<title>crj admin</title>

<meta charset="utf-8"/>

<link rel="stylesheet" type="text/css" href="files/?p=kube.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="files/?p=start.css" media="all" />
<link rel="stylesheet" type="text/css" href="files/?p=codemirror.css">
<link rel="stylesheet" type="text/css" href="files/?p=3024-night.css">

</head>

<body style="background: url(files/?p=linedpaper.png);">
	
	<div class="wrapper" style="padding: 40px 0">
	
	<div class="units-row units-split" style="margin: 0">
		
		<div class="unit-20">
			&nbsp;
		</div>
		
		<div class="unit-80">
		
		<div>&nbsp;
			<div style="position: relative;display: inline-block;float: right;position: relative;">
				<div style="position: absolute;top: -20px;right: 0;border: 1px solid #ccc;background: #fff;border-bottom: 0;padding: 10px 40px;cursor: pointer;font-size: 14px" id="switch-properties">
					Properties
				</div>
			</div>
		</div>
		
		</div>
		
    </div>
	
<div class="units-row units-split">
	
    <div class="unit-20">
		
		<div>&nbsp;
			<div style="display: inline-block;float: right;position: relative;">
				<div class="btnOn unselectable" id="switch-pages">Страницы</div>
			</div>
		</div>
		
		<br>
		
		<div>&nbsp;
			<div style="display: inline-block;float: right;position: relative;">
				<div class="btnOff unselectable" id="switch-blocks">Блоки</div>
			</div>
		</div>
		
		<br>
		
		<div>&nbsp;
			<div style="display: inline-block;float: right;position: relative;">
				<div class="btnOff unselectable" id="switch-files">Файлы</div>
			</div>
		</div>
		
    </div>
    
    <div class="unit-80">
		<div style="border: 1px solid #ccc;background: #fff;padding: 20px;" id="content">
		
		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
		
		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
		
		<p>
			<script>
				var pages = {{$SYS_ADMIN_PAGES}};
				var files = {{$SYS_ADMIN_FILES}};
			</script>
		</p>
		
		<br><br>
		
		<span class="btn" style="margin-right: 10px">Сохранить</span><span class="btn">Отменить</span>
		
		</div>
		
    </div>
</div>
	
		<!-- <footer id="footer"></footer> -->
	
	</div>

<script src="files/?p=codemirror.js"></script>
	
<script type="application/x-javascript" src="files/?p=json_parse.js"></script>
<script type="application/x-javascript" src="files/?p=yajax.js"></script>
<script type="application/x-javascript" src="files/?p=event.js"></script>
<script type="application/x-javascript" src="files/?p=main.js"></script>



</body>
</html>
