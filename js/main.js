
/**
 *
 */

crj = {}

/**
 * Get element by ID
 */

crj.id = function(a)
{
	if (a) return document.getElementById(a); else return false;
}

/**
 * Render pages 
 */

crj.renderPages = function()
{
	var out, select, option, html
	
	crj.id('content').innerHTML = ''
	
	if (typeof pages == 'object')
	{
		crj.id('content').appendChild(crj.makeSelect(pages))
		
		crj.id('content').appendChild(crj.makeButton('Создать страницу'))
		crj.id('content').appendChild(crj.makeButton('Удалить страницу'))
		
		yajax.send({
			"method": "POST",
			"url": document.URL,
			"data": {"src": 2},
			"success": function(response)
			{
				crj.id('content').appendChild(crj.makeTextfield(response, 'html-content'))
				
				var editor = CodeMirror.fromTextArea(crj.id('html-content'), {
					lineNumbers: true,
					mode: "application/x-ejs",
					indentUnit: 4,
					indentWithTabs: true,
					enterMode: "keep",
					tabMode: "shift",
					lineWrapping: true
				});
				
				crj.id('content').appendChild(crj.makeButton('Сохранить', 'btn-green'))
				crj.id('content').appendChild(crj.makeButton('Восстановить', 'btn-yellow'))
			}
		})
	}
	
}

/**
 * Render files
 */

crj.renderFiles = function()
{
	function renderFolder(name)
	{
		var el, f, out = []
		for (f in files[name])
		{
			out.push(f)
		}
		
		el = crj.id('files')
		if (el)
		{
			el.parentNode.removeChild(el)
		}
		crj.id('content').appendChild(crj.makeSelect(out, 'files'))
	}
	
	crj.id('content').innerHTML = ''
	
	var a, out = [], folder
	for (a in files)
	{
		out.push(a)
	}
	
	crj.id('content').appendChild(crj.makeSelect(out))
	renderFolder(files)
}

/**
 * Make div
 */

crj.makeDiv = function(content, id)
{
	var el, id
	
	el = document.createElement('div')
	el.id = id
	el.innerHTML = content
	return el
}
 
/**
 * Make select from data array options
 */

crj.makeSelect = function(data, id)
{
	var select, option
	
	select = document.createElement('select')
	select.className = 'input'
	if (id) select.id = id
	
	for (var key in data)
	{
		option = document.createElement('option')
		option.setAttribute('value', decodeURIComponent(data[key]))
		option.innerHTML = decodeURIComponent(data[key])
		select.appendChild(option)
	}
	
	return select
}

/**
 * Make button
 */

crj.makeButton = function(text, addClass)
{
	var btn
	
	btn = document.createElement('span')
	btn.className = 'btn crj-btn' + ' ' + addClass
	btn.innerHTML = text
	
	return btn
}

/**
 * Make textfield
 */

crj.makeTextfield = function(text, id)
{
	var input
	
	input = document.createElement("textarea")
	input.id = id
	input.maxLength = "5000"
	//input.cols = "80"
	input.style.width = '100%'
	input.rows = "30"
	input.className = 'crj-textarea'
	input.value = text
	
	return input
}

// init

var btns
var events = []

btns = {'switch-pages': 'renderPages', 'switch-blocks': 'renderBlocks', 'switch-files': 'renderFiles'}
for (var key in btns)
{
	var a = btns[key]
	Event.add(crj.id(key), 'click', function() {
		
		var el = this
		
		for (var b in btns)
		{
			if (el.id == b)
			{
				el.className = 'btnOn unselectable'
				var c = btns[b]
				window['crj'][c]()
			}
			else
			{
				crj.id(b).className = 'btnOff unselectable'
			}
		}
	})
}

crj.hello = function()
{
	alert('good day !!!')
}

crj.renderPages()
